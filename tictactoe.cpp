/**
 * Isaac Nemzer - inemzer1@jhu.edu
 * Julian Dorsey - jdorse28@jhu.edu
 * Matt Saltzman - msaltzm5@jhu.edu
 * HW5
 * 4/15/16
 * tictactoe.cpp
 */

#include "tictactoe.h"

bool TicTacToeGame::checkHori() {
	for (int a = 0; a < 3; a++) {
		if (getBoard1().getGrid()[a][0] == getBoard1().getGrid()[a][1]
				&& getBoard1().getGrid()[a][0] != '-') {
			if (getBoard1().getGrid()[a][0] == getBoard1().getGrid()[a][2]) {
				return true;
			}
		}
	}
	return false;
}

bool TicTacToeGame::checkVert() {
	for (int a = 0; a < 3; a++) {
		if (getBoard1().getGrid()[0][a] == getBoard1().getGrid()[1][a]
				&& getBoard1().getGrid()[0][a] != '-') {
			if (getBoard1().getGrid()[0][a] == getBoard1().getGrid()[2][a]) {
				return true;
			}
		}
	}
	return false;
}

bool TicTacToeGame::checkDiag() {
	if (getBoard1().getGrid()[0][0] == getBoard1().getGrid()[1][1]
			&& getBoard1().getGrid()[0][0] != '-') {
		if (getBoard1().getGrid()[1][1] == getBoard1().getGrid()[2][2]) {
			return true;
		}
	}
	if (getBoard1().getGrid()[2][0] == getBoard1().getGrid()[1][1]
			&& getBoard1().getGrid()[2][0] != '-') {
		if (getBoard1().getGrid()[1][1] == getBoard1().getGrid()[0][2]) {
			return true;
		}
	}
	return false;
}

TicTacToeGame::TicTacToeGame() {
	setBoard1(Board(3));
	createP1();
	createP2();
	setCurr(getP1());
	cout << "\n" << getCurr().getName() << "'s turn!\n" << endl;
}

TicTacToeGame::TicTacToeGame(string p1, string p2) {
	setBoard1(Board(3));
	setP1(Player(p1, 'O'));
	setP2(Player(p2, 'X'));
	setCurr(getP1());
	cout << "\n" << getCurr().getName() << "'s turn!\n" << endl;
}



void TicTacToeGame::printBoard() {
	for (int a = 0; a < 3; a++) {
		for (int b = 0; b < getBoard1().getSize(); b++) {
			cout << getBoard1().getGrid()[a][b] << " ";
		}
		cout << endl;
	}
}

bool TicTacToeGame::checkWin() {
	bool win = checkHori();
	if (!win) {
		win = checkVert();
	}
	if (!win) {
		win = checkDiag();
	}
	return win;
}

bool TicTacToeGame::checkStale() {
	for (int a = 0; a < 3; a++) {
		for (int b = 0; b < getBoard1().getSize(); b++) {
			if (getBoard1().getGrid()[a][b] == '-') {
				return false;
			}
		}
	}
	return true;
}

GameResult TicTacToeGame::attack_square(Coord coord) {
	int one = coord.first;
	int two = coord.second;
	if (one > 2 || two > 2 || one < 0 || two < 0 || getBoard1().getGrid()[one][two] != '-') {
		cout << "Invalid move! Choose another.\n" << endl;
		return RESULT_INVALID;
	}
	getBoard1().getGrid()[one][two] = getCurr().getSymbol();
	if (checkWin()) {
		if (getCurr().equals(getP1())) {
			printBoard();
			cout << getP1().getName() << " wins!\n" << endl;
			return RESULT_PLAYER1_WINS;
		} else {
			printBoard();
			cout << getP2().getName() << " wins!\n" << endl;
			return RESULT_PLAYER2_WINS;
		}
	} else if (checkStale()) {
		printBoard();
		cout << "Stalemate!" << endl;
		return RESULT_STALEMATE;
	}

	printBoard();
	if (getCurr().equals(getP1())) {
		setCurr(getP2());
	} else {
		setCurr(getP1());
	}
	cout << "\n" << getCurr().getName() << "'s turn! " << endl;

	return RESULT_KEEP_PLAYING;
}
