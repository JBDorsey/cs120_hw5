#include "tictactoe.h"
#include <iostream>

/**
 * Isaac Nemzer - inemzer1@jhu.edu
 * Julian Dorsey - jdorse28@jhu.edu
 * Matt Saltzman - msaltzm5@jhu.edu
 * HW5
 * 4/15/16
 * play_ttt.cpp
 */

using namespace std;

void game1()
{
	TicTacToeGame tttg("P1", "P2");

	// Initial state:

	// - - -
	// - - -
	// - - -

	GameResult result = tttg.attack_square(make_pair(1, 1));
	assert(result == RESULT_KEEP_PLAYING);

	// State after previous turn:

	// - - -
	// - O -
	// - - -

	result = tttg.attack_square(make_pair(0, 0));
	assert(result == RESULT_KEEP_PLAYING);

	// X - -
	// - O -
	// - - -

	// oops, already an X on this square
	result = tttg.attack_square(make_pair(0, 0));
	assert(result == RESULT_INVALID);

	// note: an invalid move doesn't count as a turn; still O's turn

	result = tttg.attack_square(make_pair(0, 1));
	assert(result == RESULT_KEEP_PLAYING);

	// X O -
	// - O -
	// - - -

	result = tttg.attack_square(make_pair(2, 1));
	assert(result == RESULT_KEEP_PLAYING);

	// X O -
	// - O -
	// - X -

	result = tttg.attack_square(make_pair(1, 0));
	assert(result == RESULT_KEEP_PLAYING);

	// X O -
	// O O -
	// - X -

	result = tttg.attack_square(make_pair(1, 2));
	assert(result == RESULT_KEEP_PLAYING);

	// X O -
	// O O X
	// - X -

	result = tttg.attack_square(make_pair(0, 2));
	assert(result == RESULT_KEEP_PLAYING);

	// X O O
	// O O X
	// - X -

	result = tttg.attack_square(make_pair(2, 0));
	assert(result == RESULT_KEEP_PLAYING);

	// X O O
	// O O X
	// X X -

	result = tttg.attack_square(make_pair(2, 2));
	assert(result == RESULT_STALEMATE);

	// X O O
	// O O X
	// X X O

	cout << "PASSED game 1" << endl;
}

void game2() {
	TicTacToeGame tttg("P1", "P2");

	// Initial state:

	// - - -
	// - - -
	// - - -

	GameResult result = tttg.attack_square(make_pair(1, 1));
	assert(result == RESULT_KEEP_PLAYING);

	// State after previous turn:

	// - - -
	// - O -
	// - - -

	result = tttg.attack_square(make_pair(0, 0));
	assert(result == RESULT_KEEP_PLAYING);

	// X - -
	// - O -
	// - - -

	// oops, already an X on this square
	result = tttg.attack_square(make_pair(0, 0));
	assert(result == RESULT_INVALID);

	// oops, negative index value
	result = tttg.attack_square(make_pair(-1, 0));
	assert(result == RESULT_INVALID);

	// oops, negative index value
	result = tttg.attack_square(make_pair(0, -1));
	assert(result == RESULT_INVALID);

	// note: an invalid move doesn't count as a turn; still O's turn

	result = tttg.attack_square(make_pair(0, 1));
	assert(result == RESULT_KEEP_PLAYING);

	// X O -
	// - O -
	// - - -

	result = tttg.attack_square(make_pair(2, 1));
	assert(result == RESULT_KEEP_PLAYING);

	// X O -
	// - O -
	// - X -

	result = tttg.attack_square(make_pair(1, 0));
	assert(result == RESULT_KEEP_PLAYING);

	// X O -
	// O O -
	// - X -

	result = tttg.attack_square(make_pair(1, 2));
	assert(result == RESULT_KEEP_PLAYING);

	// X O -
	// O O X
	// - X -

	result = tttg.attack_square(make_pair(0, 2));
	assert(result == RESULT_KEEP_PLAYING);

	// X O O
	// O O X
	// - X -

	result = tttg.attack_square(make_pair(2, 2));
	assert(result == RESULT_KEEP_PLAYING);

	// X O O
	// O O X
	// - X X

	result = tttg.attack_square(make_pair(2, 0));
	assert(result == RESULT_PLAYER1_WINS);

	// X O O
	// O O X
	// O X O

	cout << "PASSED game 2" << endl;

}
void game3() {
	TicTacToeGame tttg("P1", "P2");

	// Initial state:

	// - - -
	// - - -
	// - - -

	GameResult result = tttg.attack_square(make_pair(1, 1));
	assert(result == RESULT_KEEP_PLAYING);

	// - - -
	// - O -
	// - - -

	result = tttg.attack_square(make_pair(0, 1));
	assert(result == RESULT_KEEP_PLAYING);

	// - X -
	// - O -
	// - - -

	result = tttg.attack_square(make_pair(1, 0));
	assert(result == RESULT_KEEP_PLAYING);

	// - X -
	// O O -
	// - - -

	result = tttg.attack_square(make_pair(0, 0));
	assert(result == RESULT_KEEP_PLAYING);

	// X X -
	// O O -
	// - - -

	result = tttg.attack_square(make_pair(2, 0));
	assert(result == RESULT_KEEP_PLAYING);

	// X X -
	// O O -
	// O - -

	result = tttg.attack_square(make_pair(0, 2));
	assert(result == RESULT_PLAYER2_WINS);

	// X X X
	// O O -
	// O - -

	cout << "PASSED game 3" << endl;
}

void game4() {
	TicTacToeGame tttg("P1", "P2");

	// Initial state:

	// - - -
	// - - -
	// - - -

	GameResult result = tttg.attack_square(make_pair(0, 2));
	assert(result == RESULT_KEEP_PLAYING);

	// - - O
	// - - -
	// - - -

	result = tttg.attack_square(make_pair(0, 1));
	assert(result == RESULT_KEEP_PLAYING);

	// - X O
	// - - -
	// - - -

	result = tttg.attack_square(make_pair(1, 2));
	assert(result == RESULT_KEEP_PLAYING);

	// - X O
	// - - O
	// - - -

	result = tttg.attack_square(make_pair(1, 1));
	assert(result == RESULT_KEEP_PLAYING);

	// - X O
	// - X O
	// - - -

	result = tttg.attack_square(make_pair(2, 2));
	assert(result == RESULT_PLAYER1_WINS);

	// - X O
	// - X O
	// - - O


	cout << "PASSED game 4" << endl;
}

void game5() {
	TicTacToeGame tttg("P1", "P2");

	// Initial state:

	// - - -
	// - - -
	// - - -

	GameResult result = tttg.attack_square(make_pair(0, 2));
	assert(result == RESULT_KEEP_PLAYING);

	// - - O
	// - - -
	// - - -

	result = tttg.attack_square(make_pair(0, 0));
	assert(result == RESULT_KEEP_PLAYING);

	// X - O
	// - - -
	// - - -

	result = tttg.attack_square(make_pair(2, 1));
	assert(result == RESULT_KEEP_PLAYING);

	// X - O
	// - - -
	// - O -

	result = tttg.attack_square(make_pair(2, 2));
	assert(result == RESULT_KEEP_PLAYING);

	// X - O
	// - - -
	// - O X

	result = tttg.attack_square(make_pair(1, 0));
	assert(result == RESULT_KEEP_PLAYING);

	// X - O
	// O - -
	// - O X

	result = tttg.attack_square(make_pair(1, 1));
	assert(result == RESULT_PLAYER2_WINS);

	// X - O
	// O X -
	// - O X


	cout << "PASSED game 5" << endl;
}

int main(void)
{
	game1();
	game2();
	game3();
	game4();
	game5();
	cout << "All Tests Passed!"<< endl;
	return 0;
}
