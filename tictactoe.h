#ifndef TIC_TAC_TOE_GAME_H
#define TIC_TAC_TOE_GAME_H
/**
 * Isaac Nemzer - inemzer1@jhu.edu
 * Julian Dorsey - jdorse28@jhu.edu
 * Matt Saltzman - msaltzm5@jhu.edu
 * HW5
 * 4/15/16
 * tictactoe.h
 */

#include "game.h"

class TicTacToeGame : public Game {

private:
	bool checkHori();
	bool checkVert();
	bool checkDiag();


public:
	TicTacToeGame();
	TicTacToeGame(string p1, string p2);
	void printBoard();
	bool checkWin();
	bool checkStale();
	virtual GameResult attack_square(Coord coord);
};

#endif
