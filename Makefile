# Isaac Nemzer - inemzer1@jhu.edu
# Julian Dorsey - jdorse28@jhu.edu
# Matt Saltzman - msaltzm5@jhu.edu
# HW5
# 4/15/16
# Makefile

CXX = g++
CONSERVATIVE_FLAGS = -std=c++11 -Wall -Wextra -pedantic
DEBUGGING_FLAGS = -g -O0
CXXFLAGS = $(CONSERVATIVE_FLAGS) $(DEBUGGING_FLAGS)

.PHONY: all
all: play_ttt play_bs hw5

hw5.o: hw5.cpp game.h
	$(CXX) -c hw5.cpp $(CXXFLAGS)

play_ttt.o: play_ttt.cpp game.h
	$(CXX) -c play_ttt.cpp $(CXXFLAGS)

play_bs.o: play_bs.cpp game.h
	$(CXX) -c play_bs.cpp $(CXXFLAGS)

hw5: hw5.o tictactoe.o battleship.o game.o
	$(CXX) -o hw5 hw5.o tictactoe.o battleship.o game.o $(CXXFLAGS)

play_ttt: play_ttt.o tictactoe.o game.o
	$(CXX) -o play_ttt play_ttt.o tictactoe.o game.o $(CXXFLAGS)

tictactoe.o: tictactoe.cpp tictactoe.h game.h
	$(CXX) -c tictactoe.cpp $(CXXFLAGS)

play_bs: play_bs.o battleship.o game.o
	$(CXX) -o play_bs play_bs.o battleship.o game.o $(CXXFLAGS)

battleship.o: battleship.cpp battleship.h game.h
	$(CXX) -c battleship.cpp $(CXXFLAGS)

game.o: game.cpp game.h
	$(CXX) -c game.cpp $(CXXFLAGS)


clean:
	rm -f *.o play_ttt play_bs
