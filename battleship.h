#ifndef BATTLESHIP_H
#define BATTLESHIP_H
/**
 * Isaac Nemzer - inemzer1@jhu.edu
 * Julian Dorsey - jdorse28@jhu.edu
 * Matt Saltzman - msaltzm5@jhu.edu
 * HW5
 * 4/15/16
 * battleship.h
 */

#include "game.h"

class BattleshipGame : public Game {

private:
	std::vector<Coord> p1Ships;
	std::vector<Coord> p2Ships;

	void makeShips();
	void makeShips(std::array<Coord, 5> player1_ships,
			std::array<Coord, 5> player2_ships);

public:
	BattleshipGame();
	BattleshipGame(std::array<Coord, 5> player1_ships,
			std::array<Coord, 5> player2_ships);
	std::vector<Coord> getP1Ships();
	std::vector<Coord> getP2Ships();
	void printBoard();
	bool checkWin();
	bool checkHit(Coord coord);
	bool checkCoord(Coord coord);
	virtual GameResult attack_square(Coord coord);
};



#endif
