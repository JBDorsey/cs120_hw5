/**
 * Isaac Nemzer - inemzer1@jhu.edu
 * Julian Dorsey - jdorse28@jhu.edu
 * Matt Saltzman - msaltzm5@jhu.edu
 * HW5
 * 4/15/16
 * battleship.cpp
 */

#include "battleship.h"

BattleshipGame::BattleshipGame() {
	setBoard1(Board(10));
	setBoard2(Board(10));
	makeShips();
	createP1();
	createP2();
	setCurr(getP1());
	cout << "\n" << getCurr().getName() << "'s turn! ";
}

BattleshipGame::BattleshipGame(std::array<Coord, 5> player1_ships,
			std::array<Coord, 5> player2_ships) {
	setBoard1(Board(10));
	setBoard2(Board(10));
	makeShips(player1_ships, player2_ships);
	setP1(Player("P1"));
	setP2(Player("P2"));
	setCurr(getP1());
	cout << "\n" << getCurr().getName() << "'s turn!" << endl;
}

std::vector<Coord> BattleshipGame::getP1Ships() {
	return p1Ships;
}

std::vector<Coord> BattleshipGame::getP2Ships() {
	return p2Ships;
}

void BattleshipGame::makeShips(std::array<Coord, 5> player1_ships,
			std::array<Coord, 5> player2_ships) {
	for (int a = 0; a < 5; a++) {
		p1Ships.push_back(player1_ships.at(a));
		getBoard1().getGrid()[player1_ships.at(a).first]
			[player1_ships.at(a).second] = 'O';
		p2Ships.push_back(player2_ships.at(a));
		getBoard2().getGrid()[player2_ships.at(a).first]
			[player2_ships.at(a).second] = 'O';
	}
}

void BattleshipGame::makeShips() {
	int x;
	int y;
	int a = 0;
	while (a < 5) {
		bool exists = false;
		x = rand() % 10;
		y = rand() % 10;
		Coord coord = make_pair(x, y);
		for (unsigned int c = 0; c < p1Ships.size(); c++) {
			if (coord.first == p1Ships.at(c).first
					&& coord.second == p1Ships.at(c).second) {
				exists = true;
				break;
			}
		}
		if (!exists) {
			getBoard1().getGrid()[x][y] = 'O';
			p1Ships.push_back(coord);
			a++;
		}
	}
	int b = 0;
	while (b < 5) {
		bool exists = false;
		x = rand() % 10;
		y = rand() % 10;
		Coord coord = make_pair(x, y);
		for (unsigned int d = 0; d < p2Ships.size(); d++) {
			if (coord.first == p2Ships.at(d).first
					&& coord.second == p2Ships.at(d).second) {
				exists = true;
				break;
			}
		}
		if (!exists) {
			getBoard2().getGrid()[x][y] = 'O';
			p2Ships.push_back(coord);
			b++;
		}
	}
}

void BattleshipGame::printBoard() {
	cout << getP1().getName() << "'s board:" << endl;
	for (int a = 0; a < 10; a++) {
		for (int b = 0; b < 10; b++) {
			cout << getBoard1().getGrid()[a][b] << " ";
		}
		cout << endl;
	}
	cout << getP2().getName() << "'s board:" << endl;;
	for (int c = 0; c < 10; c++) {
		for (int d = 0; d < 10; d++) {
			cout << getBoard2().getGrid()[c][d] << " ";
		}
		cout << endl;
	}
}

bool BattleshipGame::checkWin() {
	if (p1Ships.size() == 0) {
		return true;
	}
	if (p2Ships.size() == 0) {
		return true;
	}
	return false;
}

bool BattleshipGame::checkHit(Coord coord) {
	if (getCurr().equals(getP2())) {
		for (unsigned int c = 0; c < p1Ships.size(); c++) {
			if (coord.first == p1Ships.at(c).first
					&& coord.second == p1Ships.at(c).second) {
				getBoard1().getGrid()[coord.first][coord.second] = 'H';
				p1Ships.erase(p1Ships.begin() + c);
				cout << getCurr().getName() << " hit!\n" << endl;
				return true;
			}
		}
		getBoard1().getGrid()[coord.first][coord.second] = 'M';
		cout << "\n" << getCurr().getName() << " missed.\n" << endl;
		return false;
	} else {
		for (unsigned int d = 0; d < p2Ships.size(); d++) {
			if (coord.first == p2Ships.at(d).first
					&& coord.second == p2Ships.at(d).second) {
				getBoard2().getGrid()[coord.first][coord.second] = 'H';
				p2Ships.erase(p2Ships.begin() + d);
				cout << getCurr().getName() << " hit!\n" << endl;
				return true;
			}
		}
		getBoard2().getGrid()[coord.first][coord.second] = 'M';
		cout << "\n" << getCurr().getName() << " missed.\n" << endl;
		return false;
	}
}

bool BattleshipGame::checkCoord(Coord coord) {
	int one = coord.first;
	int two = coord.second;
	if (getCurr().equals(getP2())) {
		if (getBoard1().getGrid()[one][two] == 'H'
				|| getBoard1().getGrid()[one][two] == 'M') {
			return false;
		}
	} else {
		if (getBoard2().getGrid()[one][two] == 'H'
				|| getBoard2().getGrid()[one][two] == 'M') {
			return false;
		}
	}
	return true;
}


GameResult BattleshipGame::attack_square(Coord coord) {
	int one = coord.first;
	int two = coord.second;
	if (one > 9 || two > 9 || one < 0 || two < 0 || !checkCoord(coord)) {
		cout << "\nInvalid move! Choose another. ";
		return RESULT_INVALID;
	}
	checkHit(coord);
	if (checkWin()) {
		if (getCurr().equals(getP1())) {
			printBoard();
			cout << getP1().getName() << " wins!\n" << endl;
			return RESULT_PLAYER1_WINS;
		} else {
			printBoard();
			cout << getP2().getName() << " wins!\n" << endl;
			return RESULT_PLAYER2_WINS;
		}
	}

	if (getCurr().equals(getP1())) {
		setCurr(getP2());
	} else {
		setCurr(getP1());
	}
	cout << getCurr().getName() << "'s turn! ";

	return RESULT_KEEP_PLAYING;
}
