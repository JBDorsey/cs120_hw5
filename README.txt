
 Isaac Nemzer - inemzer1@jhu.edu
 Julian Dorsey - jdorse28@jhu.edu
 Matt Saltzman - msaltzm5@jhu.edu
 HW5
 4/15/16

To make all targets:
$ make

To simply play a game of Tic Tac Toe or Battleship Lite, make all targets:
$ ./hw5
Follow onscreen instructions

To run test games of Tic Tac Toe:
$ ./play_ttt

To run test games of Battleship Lite:
$ ./play_bs
