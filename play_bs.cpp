#include "battleship.h"
#include <iostream>

/**
 * Isaac Nemzer - inemzer1@jhu.edu
 * Julian Dorsey - jdorse28@jhu.edu
 * Matt Saltzman - msaltzm5@jhu.edu
 * HW5
 * 4/15/16
 * play_bs.cpp
 */

using namespace std;

void game1()
{
	array<Coord, 5> player1_ships = {{
		make_pair(2, 2),
		make_pair(4, 4),
		make_pair(5, 5),
		make_pair(6, 6),
		make_pair(8, 8)
	}};
	array<Coord, 5> player2_ships = {{
		make_pair(2, 2),
		make_pair(4, 4),
		make_pair(5, 5),
		make_pair(6, 6),
		make_pair(8, 8)
	}};
	BattleshipGame bsg(player1_ships, player2_ships);

	GameResult result = bsg.attack_square(make_pair(2, 2)); // P1 HIT
	assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(2, 2)); // P2 HIT
	assert(result == RESULT_KEEP_PLAYING);

	// oops, already an X on this square
	result = bsg.attack_square(make_pair(2, 2)); // P1 invalid move
	assert(result == RESULT_INVALID);

	// note: an invalid move doesn't count as a turn; still player 1's turn

	result = bsg.attack_square(make_pair(4, 4)); // P1 HIT
	assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(4, 4)); // P2 HIT
	assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(5, 5)); // P1 HIT
	assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(5, 5)); // P2 HIT
	assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(6, 6)); // P1 HIT
	assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(6, 5)); // P2 MISS
	assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(8, 8)); // P1 HIT
	assert(result == RESULT_PLAYER1_WINS);

	cout << "PASSED game 1" << endl;
}

void game2()
{
	array<Coord, 5> player1_ships = {{
		make_pair(0, 0), //Hit
		make_pair(9, 0), //Hit
		make_pair(5, 5), //Hit
		make_pair(6, 6),
		make_pair(8, 8)
	}};
	array<Coord, 5> player2_ships = {{
		make_pair(0, 9), //Hit
		make_pair(9, 9), //Hit
		make_pair(5, 5), //Hit
		make_pair(6, 6), //Hit
		make_pair(8, 8)  //Hit
	}};
	BattleshipGame bsg(player1_ships, player2_ships);

	GameResult result = bsg.attack_square(make_pair(0, 10)); // P1 Invalid (Y too big)
	assert(result == RESULT_INVALID);

	result = bsg.attack_square(make_pair(0, 9)); // P1 Hits Corner
	assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(10, 0)); // P2 Invalid (X too big)
	assert(result == RESULT_INVALID);

	result = bsg.attack_square(make_pair(0, 0)); // P2 Hits Corner
	assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(0, 9)); // P1 Invalid (Already Hit Square)
	assert(result == RESULT_INVALID);

	result = bsg.attack_square(make_pair(1, 9)); // P1 Misses
	assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(9, 0)); // P2 Hits Corner
	assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(1, 9)); // P1 Invalid (Already Missed Square)
	assert(result == RESULT_INVALID);

	result = bsg.attack_square(make_pair(9, 9)); // P1 Hits Corner
	assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(9, 0)); // P2 Invalid (Already Hit Square)
	assert(result == RESULT_INVALID);

	result = bsg.attack_square(make_pair(-1, 0)); // P2 Invalid (X negative)
	assert(result == RESULT_INVALID);

	result = bsg.attack_square(make_pair(0, -1)); // P2 Invalid (Y negative)
	assert(result == RESULT_INVALID);

	result = bsg.attack_square(make_pair(5, 5)); // P2 Hits
	assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(5, 5)); // P1 Hits
	assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(6, 6)); // P2 Hits
	assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(3, 4)); // P1 Misses
	assert(result == RESULT_KEEP_PLAYING);

	result = bsg.attack_square(make_pair(8, 8)); // P2 Hits and wins
	assert(result == RESULT_PLAYER2_WINS);


}

int main(void)
{
	game1();
	game2();
	cout << "All Tests Passed!" << endl;
	return 0;
}
