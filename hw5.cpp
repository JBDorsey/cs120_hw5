#include <iostream>
#include "tictactoe.h"
#include "battleship.h"
#include <cstdio>


/**
 * Isaac Nemzer - inemzer1@jhu.edu
 * Julian Dorsey - jdorse28@jhu.edu
 * Matt Saltzman - msaltzm5@jhu.edu
 * HW5
 * 4/15/16
 * hw5.cpp
 */

using namespace std;

GameResult player_attack(Game& game) {
    Coord coord;
    int row;
    int column;
    cout << "Enter integer coordinates (ex: 1 3)" << endl;
    scanf("%d %d", &row, &column);
    coord = make_pair(row-1, column-1);
    return game.attack_square(coord);
}

int getChoice() {
    cout << "\nWhat would you like to play? Enter 1 or 2:" << endl
             << "1) Tic Tac Toe" << endl
             << "2) Battleship" << endl;
    int choice;
    cin >> choice;
    return choice;
}

void TicTacToe() {
    TicTacToeGame tttg;
    GameResult status = RESULT_KEEP_PLAYING;

    cout <<"Initial Board:" << endl;
    cout << "  1 2 3 " << endl;
    cout << "1 - - - " << endl;
    cout << "2 - - - " << endl;
    cout << "3 - - - " << endl;


    do {
        status = player_attack(tttg);
    } while(status != RESULT_PLAYER1_WINS && status != RESULT_PLAYER2_WINS
            && status != RESULT_STALEMATE);
}

void Battleship() {
    srand(time(NULL));
    BattleshipGame bsg;
    GameResult status = RESULT_KEEP_PLAYING;

    do {
        status = player_attack(bsg);
    } while(status != RESULT_PLAYER1_WINS && status != RESULT_PLAYER2_WINS
            && status != RESULT_STALEMATE);
}

int main() {
    int choice = getChoice();

    while((choice != 1) && (choice != 2) && !isalpha(choice)) {
        cout << "Invalid input, enter again." << endl;
        choice = getChoice();
    }

    if(choice == 1) {
        TicTacToe();
    } else if(choice == 2) {
        Battleship();
    }
    return 0;
}
